import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Widget timeToMake = Column (
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text('Prep Time: 5 min'),
        Text(' Cook Time: 2 min'),
      ],
    );

    Widget ingredientsTitleHeader = Container(
      alignment: Alignment.center,
      child: Text('Ingredients'),
    );

    Widget headerAndTime = Container(
      color: Colors.red,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(105.0,15.0,75.0, 15.0),
            child: ingredientsTitleHeader,
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(15.0,15.0,0,15.0),
            child: timeToMake,
          ),
        ],
      ),
    );

    Widget nutritionFacts = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('Calories: 470\n'),
        Text('Protein: 54g\n'),
        Text('Carbs: 58g'),
      ],
    );

    Widget ingredientList = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('1/2 cup Oats\n'),
          Text('1 cup Almond Milk\n'),
          Text('1 tbsp Peanut Butter\n'),
          Text('2 tbsp Chocolate Protein Powder\n'),
          Text('2 tbsp Ground Flaxseed'),
        ],
      );

    Widget nutritionFactsAndList = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(0,0,15.0, 0),
            child: ingredientList,
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(15.0,0,0,0),
            child: nutritionFacts,
          ),
        ],
      ),
    );

    Widget recipeDescription = Container(
      alignment: Alignment.center,
      child: Text('This is one of my favorite simple breakfast recipes. It looks funny, but trust me... it\'s really tasty!'),
    );

    Widget bottomSection = Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 15.0, 0, 15.0),
            child: recipeDescription,
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0,0,0, 15.0),
            child: headerAndTime,
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(15.0,0,0,0),
            child: nutritionFactsAndList,
          ),
        ],
      ),
    );

    return MaterialApp(
      title: 'Homework 2',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Chocolate Flaxseed Protein Oatmeal'),
        ),
        body: ListView(
          children: [
            Image.asset(
                'images/oatmeal.jpg',
              width: 600,
              height: 375,
              fit: BoxFit.cover,
            ),
            bottomSection
          ],
        ),
      ),
    );
  }
}